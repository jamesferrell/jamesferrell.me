<script>

    if(typeof VisualMarkdownTranslation == 'undefined') {
        var VisualMarkdownTranslation = {};
    }

    <? foreach ($translations as $key => $value): ?>
        VisualMarkdownTranslation["<?= $key ?>"] = "<?= $value ?>";
    <? endforeach ?>

</script>
