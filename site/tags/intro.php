<?

kirbytext::$tags['intro'] = array(
  'attr' => array(
    'text'
  ),
  'html' => function($tag) {

    $text = $tag->attr('intro', 'intro: the text');

    return '<p class="intro">' . $text . '</p>';

  }
);
