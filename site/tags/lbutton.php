<?

kirbytext::$tags['lbutton'] = array(
  'attr' => array(
    'text', 'link'
  ),
  'html' => function($tag) {

    $link = $tag->attr('link');
    $text = $tag->attr('button', 'button: buttontext');

    return '<a href="' . $link . '" class="button button-large">' . $text . '</a>';

  }
);
