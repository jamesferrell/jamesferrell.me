<?

// license
c::set('license', 'K2-PERSONAL-5abc44f1b1d651260220152fb51e1284');

// debug
c::set('debug', true);

// .md extension support
c::set('content.file.extension', 'md');

// smart quotes
c::set('smartypants', true);

// thumbnail driver
c::set('thumbs.driver', 'im');

// languages
c::set('languages', array(
  array(
    'code'    => 'en',
    'name'    => 'English',
    'locale'  => 'en_US',
    'default' => true,
    'url'     => '/'
  )
));

// routing
c::set('routes', array(

// rss » feed
  array(
    'pattern' => 'rss',
    'action'  => function() {
      go('feed');
    }
  ),
  array(
    'pattern' => 'resume',
    'action' => function() {
      go('content/1-about/jamesferrell-resume-2015.pdf');
    }
  )
));
