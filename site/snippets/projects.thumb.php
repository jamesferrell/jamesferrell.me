<?

// check for slug.jpg
if ($project->image('thumb.jpg')) {
  $thumb = $project->image('thumb.jpg')->url();
// check for slug.png
} elseif ($project->image('thumb.png')) {
  $thumb = $project->image('thumb.png')->url();
} else {
  $thumb = NULL;
}

// check for slug@2x.jpg
if ($project->image('thumb@2x.jpg')) {
  $thumb2x = $project->image('thumb@2x.jpg')->url();
// check for slug.png
} elseif ($project->image('thumb@2x.png')) {
  $thumb2x = $project->image('thumb@2x.png')->url();
} else {
  $thumb2x = NULL;
}

?>

<img class="thumb-img" data-src="<?= $thumb ?>" data-src2x="<?= $thumb2x ?>" sizes="100vw" alt="" id="<?= $project->slug() ?>">
<noscript>
  <img class="thumb-img" src="<?= $thumb ?>" srcset="<?= $thumb ?>, <?= $thumb2x?> 2x" sizes="100vw" alt="" id="<?= $project->slug() ?>">
</noscript>
