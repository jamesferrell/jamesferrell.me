<div class="header hero centered">
  <?= $page->text()->kirbytext() ?>
  <a class="button button-large" href="<?= $site->url() ?>/about"><?= $page->about_button() ?></a>
</div>
