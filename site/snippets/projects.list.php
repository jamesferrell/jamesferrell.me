<ul>

<? if($page->isHomePage()): ?>

  <? foreach($site->index()->filterBy('template', 'project')->visible()->limit(9) as $project): ?>
    <?= snippet('projects.card', array('project' => $project)) ?>
  <? endforeach ?>

<? elseif($page->slug() == 'projects'): ?>

  <? foreach($site->index()->filterBy('template', 'project')->visible() as $project): ?>
    <?= snippet('projects.card', array('project' => $project)) ?>
  <? endforeach ?>

<? endif ?>

</ul>
