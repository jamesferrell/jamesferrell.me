<svg version="1.1"
	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
<path fill="#7A8AA0" d="M20,0C9,0,0,9,0,20s9,20,20,20s20-9,20-20S31,0,20,0z M28.5,14L20,20.8L11.5,14H28.5z M11,14l6.6,6.4
	L11,25.3V14z M11,26.1l7.4-4.8l0,0l1.6,1.5l1.6-1.5l7.4,4.8H11z M29,25.3l-6.6-4.9L29,14V25.3z"/>
</svg>
