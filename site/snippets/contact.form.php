<form action="<?= $page->url()?>" method="post">

  <div class="grid-1-2">
    <label for="contactform-name"><span><?= $page->label_name() ?></span></label>
    <input name="name" class="required" type="text" id="contactform-name" required>
    <label class="validation validation-name" style="display: none;" for="contactform-name"><?= $page->error_name() ?></label>
  </div>

  <div class="grid-1-2">
    <label for="contactform-email"><span><?= $page->label_email() ?></span></label>
    <input name="email" class="required" type="email" id="contactform-email" inputmode="email" required>
    <label class="validation validation-email" style="display: none;" for="contactform-email"><?= $page->error_email() ?></label>
    <label class="validation validation-email-invalid" style="display: none;" for="contactform-email"><?= $page->error_email_invalid() ?></label>
  </div>

  <div class="grid-1">
    <label for="contactform-text"><span><?= $page->label_text() ?></span></label>
    <textarea name="text" class="required" id="contactform-text" required></textarea>
    <label class="validation validation-text" style="display: none;" for="contactform-text"><?= $page->error_text() ?></label>
  </div>

  <label class="u-screenreader" for="website" style="display: none;">Are you a robot? If so, fill out this field. If you're a human, skip it.</label>
  <input type="text" name="website" id="website" style="display: none;">

  <div class="grid-1-2 center-left">
    <button name="_submit" class="button" type="submit" value="<?= $form->token() ?>"<? e($form->successful(), ' disabled')?>>Send message</button>
  </div>

  <div class="grid-1-2 center-right">
    <? snippet('contact.icons') ?>
  </div>

</form>
