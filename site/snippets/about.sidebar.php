<div class="centered">

  <img class="figure avatar" src="<?= $page->url() ?>/james.jpg" alt="">

  <ul class="social-icons inline">
    <? snippet('icon', array('type' => 'twitter')) ?>
    <? snippet('icon', array('type' => 'dribbble')) ?>
    <? snippet('icon', array('type' => 'instagram')) ?>
    <? snippet('icon', array('type' => 'email')) ?>
  </ul>

</div>
