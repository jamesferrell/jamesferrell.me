
  <nav class="row" role="navigation">

    <ul class="centered inline">

      <li class="<? e($page->isHomePage(), ' active') ?>">
        <a class="uppercase" href="<? if($page->isHomePage()): echo '#top'; else: echo $site->url(); endif ?>">home</a>
      </li>

      <? foreach($pages->visible() as $menuitem): ?>

        <li class="<? e($menuitem->isOpen(), ' active') ?>">
          <a class="uppercase" href="<?
          // if the link is the current page, and has a depth of one, go to top
          if($menuitem->isOpen() && ($page->depth() == 1)):
            echo '#top';
          // otherwise, go to the page
          else:
            echo $site->url(); echo '/'; echo $menuitem;
          endif ?>">
          <?= $menuitem ?><? if($menuitem == 'about'): echo '<span class="u-screenreader"> me</span>'; endif ?></a>

        </li>

      <? endforeach ?>
    </ul>

  </nav>
