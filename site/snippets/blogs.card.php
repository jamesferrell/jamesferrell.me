<li class="figure grid-float">
  <a class="card" href="<?= $blog->url() ?>">

    <div class="card-caption">
      <p class="card-title"><?= $blog->title() ?></p>
      <p class="card-subtitle small"><?= $blog->description() ?></p>
    </div>

  </a>
</li>
