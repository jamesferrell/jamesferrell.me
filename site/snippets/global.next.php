<? if($next = $page->nextVisible()): ?>

  <nav class="next-container" role="navigation">
    <a class="nav-link next" href="<?= $next->url() ?>" accesskey="k"><span class="flip"><? snippet('icon.lang') ?></span></a>
  </nav>

<? endif ?>
