<main class="main row" role="main">

  <aside class="sidebar-left">
    <h1><?= $page->title() ?></h1>
    <? snippet('project.meta') ?>
    <?= $page->text()->kirbytext() ?>
  </aside>

  <article class="content-right gallery">
    <? foreach($page->children() as $image): ?>
      <? if($image->slug() == 'grid'): ?>

        <div class="grid-wrap">
          <? foreach($image->children() as $grid_image): ?>
            <div class="grid-2">
              <? snippet('project.img', array('image' => $grid_image)) ?>
            </div>
          <? endforeach ?>
        </div>

      <? else: ?>
        <? snippet('project.img', array('image' => $image)) ?>
      <? endif ?>

    <? endforeach ?>
  </article>

  <? snippet('global.nextprev') ?>

</main>
