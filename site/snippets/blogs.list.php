<ul>

<? if($page->isHomePage()): ?>

  <? foreach($site->index()->filterBy('template', 'blog')->visible()->limit(4)->flip() as $blog): ?>
    <?= snippet('blogs.card', array('blog' => $blog)) ?>
  <? endforeach ?>

<? elseif($page->slug() == 'blog'): ?>

  <? foreach($site->index()->filterBy('template', 'blog')->visible()->flip() as $blog): ?>
    <?= snippet('blogs.card', array('blog' => $blog)) ?>
  <? endforeach ?>

<? endif ?>

</ul>
