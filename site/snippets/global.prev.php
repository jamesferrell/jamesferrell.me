<? if($prev = $page->prevVisible()): ?>

  <nav class="prev-container" role="navigation">
    <a class="nav-link prev" href="<?= $prev->url() ?>" accesskey="j"><span><? snippet('icon.lang') ?></span></a>
  </nav>

<? endif ?>
