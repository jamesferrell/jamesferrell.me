  <footer class="fullwidth footer">

    <? snippet('global.footer.menu') ?>

    <div class="row centered colophon" role="contentinfo">
      <?= $site->colophon()->kirbytext() ?>
    </div>

  </footer>

  <?= js('assets/build/js/main.min.js') ?>

</body>
</html>
