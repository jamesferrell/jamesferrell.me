<div class="row">
  <div class="content-right">
    <h1><?= $page->title() ?></h1>
  </div>
</div>

<div class="row">
  <aside class="sidebar-left">
    <? snippet('blog.meta') ?>
    <? snippet('blog.share') ?>
  </aside>

  <article class="content-right">
    <?= $page->text()->kirbytext() ?>
  </article>
</div>
