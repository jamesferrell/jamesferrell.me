<li class="social-item social-item-<?= $type ?>">

  <? if($type == 'twitter'): ?>
    <a class="social-link social-link-<?= $type ?>" href="http://twitter.com/<?= $site->twitter() ?>">
    <? snippet('icon.twitter') ?>

  <? elseif($type == 'dribbble'): ?>
    <a class="social-link social-link-<?= $type ?>" href="http://dribbble.com/<?= $site->dribbble() ?>">
    <? snippet('icon.dribbble') ?>

  <? elseif($type == 'instagram'): ?>
    <a class="social-link social-link-<?= $type ?>" href="http://instagram.com/<?= $site->instagram() ?>">
    <? snippet('icon.instagram') ?>

  <? elseif($type == 'email'): ?>
    <a class="social-link social-link-<?= $type ?>" href="mailto:<?= $site->email() ?>">
    <? snippet('icon.email') ?>

  <? endif ?>
    </a>

</li>
