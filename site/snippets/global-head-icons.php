<?

// path to icons
$iconsPath = $site->url() . '/assets/img/favicon';

?>

<link rel="icon"                                  href="<?= $site->url() ?>/favicon.ico">
<link rel="apple-touch-icon"      sizes="57x57"   href="<?= $iconsPath ?>/apple-touch-icon-57.png">
<? /* <link rel="apple-touch-icon"      sizes="114x114" href="<?= $iconsPath ?>/apple-touch-icon-114.png"> */ ?>
<link rel="apple-touch-icon"      sizes="72x72"   href="<?= $iconsPath ?>/apple-touch-icon-72.png">
<link rel="apple-touch-icon"      sizes="144x144" href="<?= $iconsPath ?>/apple-touch-icon-144.png">
<? /* <link rel="apple-touch-icon"      sizes="60x60"   href="<?= $iconsPath ?>/apple-touch-icon-60.png"> */ ?>
<link rel="apple-touch-icon"      sizes="120x120" href="<?= $iconsPath ?>/apple-touch-icon-120.png">
<? /* <link rel="apple-touch-icon"      sizes="76x76"   href="<?= $iconsPath ?>/apple-touch-icon-76.png"> */ ?>
<link rel="apple-touch-icon"      sizes="152x152" href="<?= $iconsPath ?>/apple-touch-icon-152.png">
<? /* <link rel="apple-touch-icon"      sizes="180x180" href="<?= $iconsPath ?>/apple-touch-icon-180.png"> */ ?>
<? /* <link rel="icon" type="image/png" sizes="192x192" href="<?= $iconsPath ?>/favicon-192.png"> */ ?>
<? /* <link rel="icon" type="image/png" sizes="160x160" href="<?= $iconsPath ?>/favicon-160.png"> */ ?>
<link rel="icon" type="image/png" sizes="96x96"   href="<?= $iconsPath ?>/favicon-96.png">
<link rel="icon" type="image/png" sizes="16x16"   href="<?= $iconsPath ?>/favicon-16.png">
<link rel="icon" type="image/png" sizes="32x32"   href="<?= $iconsPath ?>/favicon-32.png">
<meta name="msapplication-TileColor" content="#fff">
<meta name="msapplication-TileImage" content="<?= $iconsPath ?>/apple-touch-icon-144.png">
