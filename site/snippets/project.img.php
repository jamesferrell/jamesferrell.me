<?

// check for slug.jpg
if ($image->image($image->slug() . '.jpg')) {
  $img = $image->image($image->slug() . '.jpg')->url();
// check for slug.png
} elseif ($image->image($image->slug() . '.png')) {
  $img = $image->image($image->slug() . '.png')->url();
} else {
  $img = NULL;
}

// check for slug@2x.jpg
if ($image->image($image->slug() . '@2x.jpg')) {
  $img2x = $image->image($image->slug() . '@2x.jpg')->url();
// check for slug@2x.png
} elseif ($image->image($image->slug() . '@2x.png')) {
  $img2x = $image->image($image->slug() . '@2x.png')->url();
} else {
  $img2x = NULL;
}

?>

<figure class="project-figure">
  <a class="project-link u-line-height-reset" href="<?= $image->url() ?>" id="<?= $image->slug() ?>">
    <div class="project-image-container">

      <img class="project-image" data-src="<?= $img ?>" data-src2x="<?= $img2x ?>" sizes="100vw" alt="">
      <noscript>
        <img class="project-image" src="<?= $img ?>" srcset="<?= $img ?>, <?= $img2x?> 2x" sizes="100vw" alt="">
      </noscript>

    </div>
  </a>
</figure>
