<li class="figure grid-1-2-3">
  <a id="<?
  if($pages->find('projects')->children()->first() == $project):
    echo 'first';
  elseif($pages->find('projects')->children()->visible()->last() == $project):
    echo 'last';
  endif ?>" class="card" href="<?= $project->url() ?>">

    <div class="card-thumb">
      <? snippet('projects.thumb', array("project" => $project)) ?>
    </div>

    <div class="card-caption">
      <p class="card-title"><?= $project->title() ?></p>
    </div>

  </a>
</li>
