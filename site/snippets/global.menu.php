<div class="nav-container">
  <nav class="site-nav" role="navigation">

    <ul class="menu centered">

      <? if($page->isHomePage()): ?>
        <li class="home-nav-item nav-item active">

        <a class="home-nav-link nav-link" href="#top">
          <span class="nav-name">James Ferrell</span>
          <span class="nav-title uppercase">Design &amp; Development</span>

          <div class="mobile-logo"></div>
        </a>

      <? else: ?>
        <li class="home-nav-item nav-item">

        <a class="home-nav-link nav-link" href="<?= $site->url() ?>" accesskey="h">
          <span class="nav-name">James Ferrell</span>
          <span class="nav-title uppercase">Design &amp; Development</span>

          <div class="mobile-logo"></div>
        </a>

      <? endif ?>
      </li>

      <? foreach($pages->visible() as $menuitem): ?>

        <li class="main-nav-item nav-item nav-<?= $menuitem ?><? e($menuitem->isOpen(), ' active') ?>">
          <a class="main-nav-link nav-link uppercase" href="<?
          // if the link is the current page, and has a depth of one, go to top
          if($menuitem->isOpen() && ($page->depth() == 1)):
            echo '#top';
          // otherwise, go to the page
          else:
            echo $site->url(); echo '/'; echo $menuitem;
          endif ?>" accesskey="<?= substr($menuitem,0,1) ?>">
          <?= $menuitem ?><? if($menuitem == 'about'): echo '<span class="u-screenreader"> me</span>'; endif ?></a>

        </li>

      <? endforeach ?>
    </ul>

  </nav>
</div>
