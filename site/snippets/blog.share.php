<p>
<a class="share share-twitter" target="_blank"
  href="https://twitter.com/share?url=<?= $page->url() ?>&text=<?= excerpt($page->title(), 60) ?>&via=<?= $site->twitter() ?>&hashtags=<?

  $topic = $page->primary_topic();
  $topic = preg_replace('/\s+/', '', $topic);
  $topic = preg_replace('/#+/', '', $topic);
  echo $topic;

  if($page->hashtag() != ''):

  $hashtag = $page->hashtag();
  $hashtag = preg_replace('/\s+/', '', $hashtag);
  $hashtag = preg_replace('/#+/', '', $hashtag);
  echo ','; echo $hashtag;

  endif ?>">
  <? snippet('icon.twitter-nobg') ?> Tweet this post</a>
</p>
