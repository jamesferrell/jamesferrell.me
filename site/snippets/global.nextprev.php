<nav class="nextprev" role="navigation">

  <div class="prev">
    <? if($prev = $page->prevVisible()): ?>
      <a class="nextprev-link uppercase" href="<?= $prev->url() ?>"><? snippet('icon.lang') ?> prev</a>
    <? else: ?>
      <a disabled="disabled" class="nextprev-link disabled uppercase"><? snippet('icon.lang') ?> prev</a>
    <? endif ?>
  </div>

  <div class="next">
    <? if($next = $page->nextVisible()): ?>
      <a class="nextprev-link uppercase" href="<?= $next->url() ?>">next <? snippet('icon.lang') ?></a>
    <? else: ?>
      <a disabled="disabled" class="nextprev-link disabled uppercase">next <? snippet('icon.lang') ?></a>
    <? endif ?>
  </div>

</nav>
