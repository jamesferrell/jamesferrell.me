<?

return function($site, $pages, $page) {

  $form = uniform('contact-form', array(
      'required' => array(
        'name' => '',
        'email' => 'email',
        'text' => '',
      ),
      'actions'  => array(
        array(
          '_action' => 'email',
          'to'      => 'hello@jamesferrell.me',
          'sender'  => 'hello@jamesferrell.me',
        )
      )
    )
  );

  return compact('form');
};
