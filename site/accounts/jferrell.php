<? if(!defined('KIRBY')) exit ?>

username: jamesferrell
email: hello@jamesferrell.me
password: >
  $2a$10$fuBOAa8TolUzQUyVIR8HLecICpzDiCUNbmrClyolNu5DkxG57i6Ru
language: en
role: admin
history:
  - projects/writesomething
  - about
  - blog/brackets
  - projects/berman
  - projects/blackkey
