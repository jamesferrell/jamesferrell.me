<? snippet('global.head') ?>

<main class="main fullwidth" role="main">
  <div class="row has-grid-float">

    <?= $page->text()->kirbytext() ?>

    <? snippet('blogs.list') ?>

  </div>
</main>

<? snippet('global.footer') ?>
