<? snippet('global.head') ?>

<div class="fullwidth">
  <div class="row centered">
    <main class="main" role="main">

      <? snippet('global.header') ?>
      <?= $page->text()->kirbytext() ?>

    </main>
  </div>
</div>

<? snippet('global.footer') ?>
