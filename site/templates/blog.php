<? snippet('global.head') ?>

<main class="main fullwidth u-padding-bottom-off" role="main">

  <? snippet('global.prev') ?>
  <? snippet('blog.content') ?>
  <? snippet('global.nextprev') ?>

</main>

<? snippet('global.footer') ?>
