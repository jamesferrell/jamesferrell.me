<? snippet('global.head') ?>

<main class="main fullwidth" role="main">
  <div class="row">
    <div id="contactform">

      <? if($form->hasMessage()): ?>
          <? if($form->successful()): ?>
            <div class="message-success">
              <div class="row centered">
                <?= kirbytext($page->success()) ?>
                <?= kirbytext($page->blog()) ?>
                <? snippet('contact.icons') ?>
              </div>
            </div>
          <? else: ?>
          <div class="message-fail">
            <div class="row centered">
              <?= kirbytext($page->fail()) ?>
              <? snippet('contact.icons') ?>
            </div>
          </div>
        <? endif ?>
      <? else: ?>

        <?= snippet('contact.form') ?>

      <? endif; ?>

    </div>
  </div>
</main>

<? snippet('global.footer') ?>
