<? snippet('global.head') ?>

<main class="main fullwidth" role="main">
  <? if(($page->intendedTemplate() == '')): ?>

      <div class="row centered">

        <? snippet('global.header') ?>
        <?= $pages->find('error')->text()->kirbytext() ?>

      </div>

  <? else: ?>

    <div class="main row">
      <?= $page->text()->kirbytext() ?>
    </div>

  <? endif ?>

</main>

<? snippet('global.footer') ?>
