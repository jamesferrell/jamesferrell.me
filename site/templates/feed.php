<?

    $items = $pages->find('blog')->children()->visible()->flip()->limit(10);

    snippet('feed', array(
      'link'  => url('blog'),
      'items' => $items,
      'descriptionField'  => 'description',
      'descriptionLength' => 150
    ));

?>
