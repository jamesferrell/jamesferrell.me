<?

// check for slug.jpg
if ($page->image($page->slug() . '.jpg')) {
  $img = $page->image($page->slug() . '.jpg')->url();
// check for slug.png
} elseif ($page->image($page->slug() . '.png')) {
  $img = $page->image($page->slug() . '.png')->url();
} else {
  $img = NULL;
}

// check for slug@2x.jpg
if ($page->image($page->slug() . '@2x.jpg')) {
  $img2x = $page->image($page->slug() . '@2x.jpg')->url();
// check for slug@2x.png
} elseif ($page->image($page->slug() . '@2x.png')) {
  $img2x = $page->image($page->slug() . '@2x.png')->url();
} else {
  $img2x = NULL;
}

?>


<? snippet('global.head') ?>

<main class="main fullwidth" role="main">
  <div class="row">

    <img class="main-img" data-src="<?= $img ?>" data-src2x="<?= $img2x ?>" sizes="100vw" alt="">
    <noscript>
      <img class="main-img" src="<?= $img ?>" srcset="<?= $img ?>, <?= $img2x?> 2x" sizes="100vw" alt="">
    </noscript>


    <div class="prev">
      <? if($page->parent()->parent()->slug() != 'projects'): ?>
        <a class="back uppercase" href="<?= $page->parent()->parent()->url() ?>"><? snippet('icon.lang') ?> back to project</a>
      <? else: ?>
        <a class="back uppercase" href="<?= $page->parent()->url() ?>"><? snippet('icon.lang') ?> back to project</a>
      <? endif ?>
    </div>

  </div>
</main>

<? snippet('global.footer') ?>
