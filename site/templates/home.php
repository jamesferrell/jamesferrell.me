<? snippet('global.head') ?>

<div class="fullwidth">
  <main class="main" role="main">

    <div class="row">
      <? snippet('home.header') ?>
    </div>

    <div class="row">
      <?= kirbytext($page->projects()) ?>
      <? snippet('projects.list') ?>
      <a class="button" href="<?= $site->url() ?>/projects"><?= $page->projects_button() ?></a>
    </div>

    <div class="row has-grid-float">
      <?= kirbytext($page->blog()) ?>
      <? snippet('blogs.list') ?>
      <? if($pages->find('blog')->hasChildren() > 4): ?>
        <a class="button" href="<?= $site->url() ?>/blog"><?= $page->blog_button() ?></a>
      <? endif ?>
    </div>

    <div class="row centered">
      <?= kirbytext($page->contact()) ?>
      <a class="button button-large" href="<?= $site->url() ?>/contact"><?= $page->contact_button() ?></a>
    </div>

  </div>

  </main>
</div>

<? snippet('global.footer') ?>
