<? snippet('global.head') ?>

<div class="fullwidth">
  <main class="main row" role="main">

    <?= $page->text()->kirbytext() ?>

    <? snippet('projects.list') ?>

  </main>
</div>

<? snippet('global.footer') ?>
