<? snippet('global.head') ?>

<div class="fullwidth">
  <main class="main" role="main">
    <div class="row">

      <aside class="sidebar-left">
        <? if($page == 'about'): snippet('about.sidebar'); endif ?>
      </aside>

      <article class="content-right">
        <?= $page->text()->kirbytext() ?>
      </article>

    </div>
  </main>
</div>

<? snippet('global.footer') ?>
