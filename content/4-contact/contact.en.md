Title: Contact

----

Headline: 

----

Label-name: Your Name

----

Label-email: Email Address

----

Label-text: What's up?

----

Error-name: What about your name?

----

Error-text: I feel like there's something you're not telling me.

----

Error-email: But how will I get back to you?

----

Error-email-invalid: That's not an actual email address.

----

Success: 

##Nailed it!

I'll get back to you soon. In the meantime, how about reading some (link: blog text: things I've written)?

----

Fail: 

##I blew it.

Sorry, something got messed up. Forms are hard. (email: hello@jamesferrell.me text: Email me maybe)?