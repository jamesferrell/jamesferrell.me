Title: Controller Illustrations

----

Role: design

----

Industry: gaming

----

Text: 

I recreated some of my favorite video game controllers in Photoshop for fun. Shown here:

1. (link: #nes text: NES)
2. (link: #snes text: Super NES)
3. (link: #genesis text: Genesis)
4. (link: #ps2 text: Playstation 2)
5. (link: #n64 text: Nintendo 64)
6. (link: #dreamcast text: Dreamcast)