Title: Black Key Recording

----

Role: design,development

----

Industry: audio

----

Text: 

Black Key Recording is a home recording studio in Tampa, FL.

I designed the (link: #logo text: visual identity) and (link: #website text: responsive one-pager). The website was built in KirbyCMS so that the content can easily be kept fresh as the studio grows.

View the live site at (link: http://blackkeyrecording.com text: blackkeyrecording.com).