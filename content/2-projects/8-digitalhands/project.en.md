Title: Digital Hands

----

Role: dev

----

Industry: technology

----

Text: 

Digital Hands provides white label digital security services to big companies. 

I developed two standalone landing pages to help them recruit the right people. Shown here are the (link: #website1 text: Operations page) and the (link: #website2 text: Architects page).