Title: Berman Advertising

----

Role: design,dev

----

Industry: advertising,marketing

----

Text: 

Berman Advertising is the Boston ad agency I work for. When I joined, we had an outdated site that didn't reflect the current work, staff, and philosophies of the agency. So I worked closely with creative director (link: http://bermanadvertising.com/about/people/jberman text: Jeff Berman) and art director (link: http://bermanadvertising.com/about/people/jberman text: Erin Eby) to establish the minimalist visual feel, and built the site from the ground up with (link: http://getkirby.com text: Kirby CMS). 

Shown here are the (link: #website1 text: home page), a (link: #website2 text: work page), and a (link: #website3 text: blog page).

View the live site at (link: http://bermanadvertising.com text: bermanadvertising.com).