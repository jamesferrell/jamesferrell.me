Title: Strategic Media

----

Role: design,dev

----

Industry: marketing

----

Text: 

Strategic Media LLC is a partner marketing firm based in New York. They connect the right magazines with the right people. A customized Wordpress theme made the most sense given their budget.

Shown here are the (link: #website1 text: home page) and the (link: #website2 text: clients page).

View the live site at (link: http://strategicmediallc.com text: strategicmedia.com).