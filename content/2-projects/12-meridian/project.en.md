Title: Meridian

----

Role: design,development

----

Industry: audio

----

Text: 

Meridian is my instrumental math rock duo with the super talented (link: http://soundcloud.com/benberonda text: Ben Beronda). I designed a (link: #logo text: logo) and brand identity that reflect the cerebral, angular nature of the music, and built a (link: #website text: responsive one-pager) to serve as a social media hub.

View the live site at (link: http://meridianmusic.net text: meridicanmusic.net).

As an aside, we recorded live drums for a full length album in May 2014, and we've been chipping away at it in our free time. It'll eventually be released, but under a different band name.