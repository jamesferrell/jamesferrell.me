Title: Beacon

----

Role: design

----

Industry: healthcare

----

Text: 

Beacon Health Strategies uses data to get people the amount of care they need — no more, no less. A little efficiency goes a long way when it comes to healthcare. 

I redesigned their site in the context of a brand refresh, and development was handled by a third party. Shown here is the (link: #website text: home page).

View the live site at (link: http://beaconhealthstrategies.com text: beaconhealthstrategies.com).