Title: WriteSomething

----

Text: 

WriteSomething is the brainchild of Berman art director (link: http://bermanadvertising.com/about/people/eeby text: Erin Eby). It's a holiday story that anyone can contribute to, and proceeds go to (link: http://www.826boston.org/ text: 826 Boston), a nonprofit organization that teaches kids to write.

In 2015, the conversion goal had shifted from dollars to engagement. With this in mind, I redesigned the experience from the ground up, making it as frictionless as possible. I recommended splitting the story into self-contained chapters, making it free to contribute, and auto-publishing entries in real time. I also redesigned (link: #logo text: the logo) so that it better communicates what WriteSomething is about, with visual elements that tie into the interface. Shown here:

1. (link: #chapter text: active chapter view)
2. (link: #new_entry text: new entry view)
3. (link: #published text: published entry view)
4. (link: #home text: the home page)
5. (link: #admin text: admin panel)
6. (link: #admin--entries text: admin panel, chapter entry list view)
7. (link: #admin--entry text: admin panel, entry edit view)

I worked closely with a back-end developer from (link: http://insitecreative.com  text: Insite Creative) in order to get the web app up and running. 

View (and contribute to!) the live site at (link: http://writesomething.org text: writesomething.org).

----

Role: dev,design

----

Industry: education,nonprofit