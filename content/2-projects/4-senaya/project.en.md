Title: Senaya

----

Role: design,dev

----

Industry: industrial,technology

----

Text: 

Senaya uses sensors and software to track assets intelligently. 

I built their website on a heavily customized Wordpress theme. Shown here are the (link: #website text: home page), (link: #iconography text: custom iconography), and a (link: #process text: process graphic).

View the live site at (link: http://senaya.com text: senaya.com).