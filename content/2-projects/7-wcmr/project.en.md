Title: WestCMR

----

Role: design

----

Industry: healthcare,ecommerce

----

Text: 

WestCMR buys and sells medical surplus supplies. They're also big on company culture.

I designed various (link: #website text: pages) and (link: #styleguide text: elements) for their site. Development was handled by a third party.

View the live site at (link: http://westcmr.com text: westcmr.com).