Title: Fusion Lamps

----

Role: design

----

Industry: industrial

----

Text: 

In 2012 I worked as an in-house designer for a company called Cefco, which branded and packaged various electrical products for in-house distribution of a bigger company. One of these brands was Fusion Lamps. Lacking creative direction, the brand became convoluted over time.

I redesigned the (link: #logo text: logo), (link: #colors text: brand color palette), (link: #display text: packaging), and (link: #website text: website), with a focus on usability.