Title: Ambushed

----

Role: design

----

Industry: audio

----

Text: Ambushed is my Boston-based alternative rock band. In addition to arranging and engineering the songs, I created our (link: #identity text: visual identity), collaborated with our super talented bassist/illustrator (link: http://el-cazador.tumblr.com/tagged/illustration text: Hunter Schulz) to design our (link: #album text: album art), and designed a series of (link: #flyer1 text: flyers) to promote our shows. I also designed a (link: #process text: songwriting process poster) to help keep things moving forward.