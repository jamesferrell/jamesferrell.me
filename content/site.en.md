Title: James Ferrell

----

Author: James Ferrell

----

Description: Portfolio site for James Ferrell, a designer and front-end developer in Boston, MA.

----

Keywords: visual,design,development,dev,responsive,frontend,front-end,kirby,boston,freelance,designer

----

Copyright: I started doing stuff like this in 2010, and now it’s (date: Year).

----

Colophon: This website was designed in (link: http://bohemiancoding.com/sketch/ text: Sketch) and built from the ground up with (link: http://getkirby.com text: Kirby). Repo at (link: https://bitbucket.org/JamesFerrell/jamesferrell.me text: Bitbucket).

----

Twitter: jamesferrelldd

----

Dribbble: jamesferrelldd

----

Instagram: jamesferrelldd

----

Email: hello@jamesferrell.me