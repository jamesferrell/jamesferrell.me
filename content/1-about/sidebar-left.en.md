Title: About

----

Text:

# Hey, thanks for stopping by

I'm a digital designer for (link: projects/berman text: Berman Advertising) in Boston, MA. When I’m not designing or developing, I’m usually making music or playing video games. But this website is about my design and development work, and mostly not that other stuff.

Here's my (file: jamesferrell-resume-2015.pdf text: resumé) in case you want it.

## My philosophy: design with the user in mind

My design work isn’t for me, and it isn’t for you, either. It’s for the user — the person interacting with the design.

Good design means making things intuitive. It means using helpful language and cutting out fluff and acronyms. It means the surprise and delight stuff, but only if the context is right. It means making things as simple as possible without sacrificing usability.

Design is a decision-making process, and the decisions I make are about (link: http://mattgemmell.com/respect-metrics/ text: respecting the user), whether I'm pushing pixels or writing code.
