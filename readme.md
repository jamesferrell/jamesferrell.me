# jamesferrell.me

This website was designed in (link: http://bohemiancoding.com/sketch/ text: Sketch) and built with (link: http://getkirby.com text: Kirby) and (link: http://sass-lang.com text: Sass). Repo at (link: https://bitbucket.org/JamesFerrell/jamesferrell.me text: Bitbucket).
